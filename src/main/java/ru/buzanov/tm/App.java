package ru.buzanov.tm;

import ru.buzanov.tm.dao.ProjectManager;
import ru.buzanov.tm.dao.TaskManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Task Manager
 */

public class App {
    // константы с названиями команд
    final static String CLEAR_PROJECT = "project-clear";
    final static String CREATE_PROJECT = "project-create";
    final static String LIST_PROJECT = "project-list";
    final static String REMOVE_PROJECT = "project-remove";
    final static String RENAME_PROJECT = "project-rename";
    final static String VIEW_PROJECT = "project-view";
    final static String CLEAR_TASK = "task-clear";
    final static String CREATE_TASK = "task-create";
    final static String LIST_TASK = "task-list";
    final static String REMOVE_TASK = "task-remove";
    final static String RENAME_TASK = "task-rename";
    final static String EXIT = "exit";
    final static String HELP = "help";

    // переменные
    static boolean out = true;
    static String buffer = "";
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static ProjectManager projectManager = new ProjectManager();
    private static TaskManager taskManager = new TaskManager();


    // основной метод
    public static void main(String[] args) throws IOException {
        // приветствие
        System.out.println("***WELCOME TO TASK MANAGER***");

        // основной цикл
        while (out) {
            buffer = reader.readLine();

            switch (buffer) {
                // создание проекта
                case (CREATE_PROJECT):
                    projectManager.createProject();
                    break;

                // удаление всех проектов
                case (CLEAR_PROJECT):
                    projectManager.clearProject();
                    break;

                // вывод списка проектов
                case (LIST_PROJECT):
                    projectManager.listProject();
                    break;

                // переименование проекта по индексу
                case (RENAME_PROJECT):
                    projectManager.renameProject();
                    break;

                // вывод списка проектов
                case (REMOVE_PROJECT):
                    projectManager.removeProject();
                    break;

                // вывод информации о проекте
                case (VIEW_PROJECT):
                    projectManager.viewProject();
                    break;

                // создание задачи
                case (CREATE_TASK):
                    taskManager.createTask();
                    break;

                // удаление всех задач
                case (CLEAR_TASK):
                    taskManager.clearTask();
                    break;

                // вывод списка всех задач
                case (LIST_TASK):
                    taskManager.listTask();
                    break;

                // переименование задачи по индексу
                case (RENAME_TASK):
                    taskManager.renameTask();
                    break;

                // удаление задачи по индексу
                case (REMOVE_TASK):
                    taskManager.removeTask();
                    break;

                // вывод списка комманд
                case (HELP):
                    helpOut();
                    break;

                // выход из программы
                case (EXIT):
                    out = false;
                    break;
            }
        }
    }

    // метод для вывода списка допустимых комманд
    public static void helpOut() {
        System.out.println(HELP + ": Show all commands.");
        System.out.println(CLEAR_PROJECT + ": Remove all projects.");
        System.out.println(CREATE_PROJECT + ": Create new project.");
        System.out.println(LIST_PROJECT + ": Show all projects.");
        System.out.println(REMOVE_PROJECT + ": Remove selected project.");
        System.out.println(RENAME_PROJECT + ": Rename project");
        System.out.println(VIEW_PROJECT + ": View project information");
        System.out.println(CLEAR_TASK + ": Remove all tasks.");
        System.out.println(CREATE_TASK + ": Create new task.");
        System.out.println(LIST_TASK + ": Show all tasks.");
        System.out.println(REMOVE_TASK + ": Remove selected tasks.");
        System.out.println(RENAME_TASK + ": Rename task");
        System.out.println(EXIT + ": Close program");
    }
}
