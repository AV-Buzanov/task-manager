package ru.buzanov.tm.dao;

import ru.buzanov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TaskManager {
    private static List<Task> taskList = new ArrayList<>();
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static ProjectManager projectManager = new ProjectManager();
    private static int indexBuf;

    // метод для создания задачи
    public void createTask() throws IOException {
        String buffer = "";
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        buffer = reader.readLine();
        System.out.println("CHOOSE PROJECT FOR TASK:");
        for (int i = 0; i < projectManager.getProjectList().size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectManager.getProjectList().get(i).getProjectName());
        }

        try {
            indexBuf = Integer.parseInt(reader.readLine());
            Task task = new Task(buffer);
            task.setProjectId(projectManager.getProjectList().get(indexBuf).getProjectId());
            taskList.add(task);
            projectManager.getProjectList().get(indexBuf).addTask(task.getTaskId());
            System.out.println("[OK]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Project with index doesn't exist!");
        }
    }

    // метод для удаления всех задач
    public void clearTask() {
        taskList.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    // метод для вывода списка задач
    public void listTask() {
        System.out.println("[TASK LIST]");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(taskList.get(i).getTaskName());
        }
    }

    // метод для переименования задачи по индексу
    public void renameTask() throws IOException {
        System.out.println("[RENAME TASK]");
        System.out.println("Choose index number of project to rename");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(taskList.get(i).getTaskName());
        }
        try {
            indexBuf = Integer.parseInt(reader.readLine());
            System.out.println("Write new name");
            taskList.get(indexBuf).setTaskName(reader.readLine());
            System.out.println("[TASK RENAMED]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Task with index doesn't exist!");
        }
    }

    // метод для удаления задачи по индексу
    public void removeTask() throws IOException {
        System.out.println("[REMOVE TASK]");
        System.out.println("Choose index number of task to remove");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(taskList.get(i).getTaskName());
        }

        try {
            taskList.remove(Integer.parseInt(reader.readLine()));
            System.out.println("[TASK REMOVED]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Task with index doesn't exist!");
        }
    }
}
