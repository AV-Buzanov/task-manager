package ru.buzanov.tm.dao;

import ru.buzanov.tm.entity.Project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProjectManager {
    private static List<Project> projectList = new ArrayList<>();
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static int indexBuf;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy", Locale.ENGLISH);

    public List<Project> getProjectList() {
        return projectList;
    }

    // метод для создания проекта
    public void createProject() throws IOException {
        Project project = new Project();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        project.setProjectName(reader.readLine());
        System.out.println("ENTER PROJECT START DATE (DD MM YYYY):");
        try {
            project.setStartDate(dateFormat.parse(reader.readLine()));
            System.out.println("ENTER PROJECT END DATE (DD MM YYYY):");
            project.setEndDate(dateFormat.parse(reader.readLine()));
            projectList.add(project);
            project=null;
            System.out.println("[OK]");
        } catch (ParseException e) {
            System.out.println("Wrong format, try again!");
        }
    }

    // метод для удаления всех проектов
    public void clearProject() {
        projectList.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    // метод для вывода списка проектов
    public void listProject() {
        System.out.println("[PROJECT LIST]");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectList.get(i).getProjectName());
        }
    }

    // метод для переименования проекта по индексу
    public void renameProject() throws IOException {
        System.out.println("[RENAME PROJECT]");
        System.out.println("Choose index number of project to rename");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectList.get(i).getProjectName());
        }
        try {
            indexBuf = Integer.parseInt(reader.readLine());
            System.out.println("Write new name");
            projectList.get(indexBuf).setProjectName(reader.readLine());
            System.out.println("[PROJECT RENAMED]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Project with index doesn't exist!");
        }
    }

    // метод для удаления проекта по индексу
    public void removeProject() throws IOException {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Choose index number of project to remove");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectList.get(i).getProjectName());
        }

        try {
            projectList.remove(Integer.parseInt(reader.readLine()));
            System.out.println("[PROJECT REMOVED]");
        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Project with index doesn't exist!");
        }
    }

    // метод для выводв информации о проекте
    public void viewProject() throws IOException {
        System.out.println("[VIEW PROJECT]");
        System.out.println("Choose index number of project");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.print(i + ". ");
            System.out.println(projectList.get(i).getProjectName());
        }

        try {
            indexBuf = Integer.parseInt(reader.readLine());
            System.out.println("PROJECT START DATE:");
            System.out.println(dateFormat.format(projectList.get(indexBuf).getStartDate()));
            System.out.println("PROJECT END DATE:");
            System.out.println(dateFormat.format(projectList.get(indexBuf).getEndDate()));

        } catch (NumberFormatException e) {
            System.out.println("Wrong input, try again!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Project with index doesn't exist!");
        }
    }

}
