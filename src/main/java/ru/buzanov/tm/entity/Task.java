package ru.buzanov.tm.entity;

import java.util.Date;

public class Task {

    // конструкторы
    public Task(String projectName) {
        this.taskName = projectName;
    }
    public Task() {
    }

    // переменные
    private String taskName;
    private String taskId;
    private String taskContent;
    private Date startDate;
    private Date endDate;
    private String projectId;

    // сеттеры и геттеры
    public void setProjectId(String projectId) { this.projectId = projectId; }

    public String getProjectId() { return projectId; }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public void setTaskContent(String taskContent) { this.taskContent = taskContent; }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getTaskContent() {
        return taskContent;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
