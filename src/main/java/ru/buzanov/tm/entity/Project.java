package ru.buzanov.tm.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Project {
    // конструкторы
    public Project(String projectName) {
        this.projectName = projectName;
    }
    public Project() {
    }

    // переменные
    private String projectName;
    private String Id;
    private String content;
    private Date startDate;
    private Date endDate;
    private List<String> tasksIdList = new ArrayList<>();

    // сеттеры и геттеры
    public List<String> getTasksIdList() { return tasksIdList; }

    public void addTask(String taskId) { this.tasksIdList.add(taskId); }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getProjectId() {
        return Id;
    }

    public String getContent() {
        return content;
    }

    public void setProjectId(String id) {
        this.Id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }


}
